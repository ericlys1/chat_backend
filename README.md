
### `sets global variables `
create an .env file in the root folder following the .env.example template

### `dev`
Run the application in development mode with 
'yarn dev:server'.
### `build`
Run the application in production mode with code transpiled in JavaScript
'yarn build'.
'node dist/server.js'
