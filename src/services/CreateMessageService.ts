import { getCustomRepository, getRepository } from 'typeorm';
import Chat from '../models/Chat';
import Message from '../models/Message';
import UsersRepository from '../repositories/UsersRepository';

interface Request {
  chat_id: string;
  user_id: string;
  message: string;
}

class CreateMessageService {
  public async execute({
    chat_id,
    user_id,
    message,
  }: Request): Promise<Message> {
    const usersRepository = getCustomRepository(UsersRepository);

    const user = await usersRepository.findOne({ where: { id: user_id } });

    if (!user) {
      throw new Error('Invalid user.');
    }

    const chatRepository = getRepository(Chat);

    const chat = await chatRepository.findOne({ where: { id: chat_id } });

    if (!chat) {
      throw new Error('Invalid chat.');
    }

    const messageRepository = getRepository(Message);

    const mess = await messageRepository.save({
      text: message,
      chat,
      author: user,
    });
    return mess;
  }
}

export default CreateMessageService;
