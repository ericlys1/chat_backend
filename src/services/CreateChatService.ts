import { getRepository } from 'typeorm';

import Chat from '../models/Chat';
import User from '../models/User';

interface Request {
  user1_id: string;
  user2_id: string;
}

class CreateChatService {
  public async execute({ user1_id, user2_id }: Request): Promise<Chat> {
    const usersRepository = getRepository(User);
    const User1 = await usersRepository.findOne({
      where: { id: user1_id },
    });
    const User2 = await usersRepository.findOne({
      where: { id: user2_id },
    });

    if (!User2) {
      throw new Error('User does not exist.');
    }

    // check if chat already exists

    const chats = await getRepository(Chat)
      .createQueryBuilder('chats')
      .innerJoinAndSelect('chats.users', 'cuu')
      .where('cuu.id = :id1 OR cuu.id = :id2', { id1: user1_id, id2: user2_id })
      .getMany();

    const chat = chats.find(chat => {
      if (chat.users[1]) {
        return chat;
      }
    });

    if (chat) {
      return chat;
    }

    // create new chat
    const newchat = await getRepository(Chat).save({
      users: [User1, User2],
    });

    return newchat;
  }
}

export default CreateChatService;
