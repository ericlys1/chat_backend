import { Router } from 'express';
import { getCustomRepository, getRepository } from 'typeorm';
import User from '../models/User';
import UsersRepository from '../repositories/UsersRepository';

import CreateUserService from '../services/CreateUserService';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';

const usersRouters = Router();

usersRouters.post('/', async (request, response) => {
  try {
    const { name, email, password } = request.body;

    const createUser = new CreateUserService();

    const user = await createUser.execute({
      name,
      email,
      password,
    });

    return response.json(user);
  } catch (err) {
    return response.status(400).json({ error: err.message });
  }
});

usersRouters.get('/', ensureAuthenticated, async (request, response) => {
  const users = await getRepository(User).find();
  users.filter(user => delete user.password);
  return response.json(users);
});

usersRouters.post('/email', ensureAuthenticated, async (request, response) => {
  const { email } = request.body;
  const user = await getCustomRepository(UsersRepository).findByEmail(email);
  if (user) delete user.password;
  return response.json(user);
});

export default usersRouters;
