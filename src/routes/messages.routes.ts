import { Router } from 'express';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';
import CreateMessageService from '../services/CreateMessageService';

const messagesRouters = Router();

messagesRouters.post('/', ensureAuthenticated, async (request, response) => {
  try {
    const { chat_id, message } = request.body;
    const { id } = request.user;

    const createMessage = new CreateMessageService();
    const newmessage = await createMessage.execute({
      chat_id,
      user_id: id,
      message,
    });

    return response.json(newmessage);
  } catch (err) {
    return response.status(400).json({ error: err.message });
  }
});

export default messagesRouters;
