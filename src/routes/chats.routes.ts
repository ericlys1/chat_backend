import { Router } from 'express';
import { getRepository } from 'typeorm';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';
import Message from '../models/Message';
import CreateChatService from '../services/CreateChatService';

const chatsRouters = Router();

chatsRouters.post('/', ensureAuthenticated, async (request, response) => {
  try {
    const { receiver_id } = request.body;
    const { id } = request.user;

    const createChat = new CreateChatService();

    const newChat = await createChat.execute({
      user1_id: id,
      user2_id: receiver_id,
    });

    return response.json(newChat);
  } catch (err) {
    return response.status(400).json({ error: err.message });
  }
});

chatsRouters.post('/messages', async (req, res) => {
  const { chat_id } = req.body;
  try {
    const messageRepository = getRepository(Message);
    const messages = await messageRepository.find({
      relations: ['author'],
      where: { chat: chat_id },
      order: { created_at: 'DESC' },
    });
    return res.json(messages);
  } catch (err) {
    return res.status(400).json({ error: err.message });
  }
});

export default chatsRouters;
