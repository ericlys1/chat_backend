import { Router } from 'express';
import usersRouter from './users.routes';
import sessionsRouter from './sessions.routes';
import chatsRouter from './chats.routes';
import messagesRouter from './messages.routes';

const routes = Router();
routes.use('/sessions', sessionsRouter);
routes.use('/users', usersRouter);
routes.use('/chats', chatsRouter);
routes.use('/messages', messagesRouter);
export default routes;
