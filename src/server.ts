import express from 'express';
import io from 'socket.io';
import { createServer } from 'http';
import cors from 'cors';
import { getRepository } from 'typeorm';
import 'dotenv/config';

import routes from './routes';

import './database';
import CreateMessageService from './services/CreateMessageService';
import Message from './models/Message';

const app = express();
const server = createServer(app);
const socketIO = io(server, { serveClient: false });

async function previusMessages(chat: string) {
  const messageRepository = getRepository(Message);
  const messages = await messageRepository.find({
    relations: ['author'],
    where: { chat },
    order: { created_at: 'DESC' },
  });
  return messages;
}

socketIO.on('connection', socket => {
  console.log(`connected...${socket.id}`);

  socket.on('previusChat', async data => {
    const chat_id = data;
    const previusMessagess = await previusMessages(chat_id);
    socket.join(chat_id);
    socket.emit('previusMessages', previusMessagess);
  });

  socket.on('sendMessage', async data => {
    const { chat_id, author_id, message } = data;
    const createMessage = new CreateMessageService();
    const newmessage = await createMessage.execute({
      chat_id,
      user_id: author_id,
      message,
    });
    socket.to(chat_id).emit('newmessage', newmessage);
  });

  socketIO.on('disconnect', () => {
    console.log('Client disconnected');
  });
});

app.use(cors());
app.use(express.json());
app.use(routes);

server.listen(3333, () => {
  console.log('🚀 Server started on port 3333!');
});
