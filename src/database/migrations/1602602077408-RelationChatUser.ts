import {MigrationInterface, QueryRunner} from "typeorm";

export class RelationChatUser1602602077408 implements MigrationInterface {
    name = 'RelationChatUser1602602077408'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "chats_users_users" ("chatsId" uuid NOT NULL, "usersId" uuid NOT NULL, CONSTRAINT "PK_8227865724042418a8c1ceada56" PRIMARY KEY ("chatsId", "usersId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_3d891de1ee6dc86bb9d6c9f044" ON "chats_users_users" ("chatsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_91c62ffedcb3d34053b698b56e" ON "chats_users_users" ("usersId") `);
        await queryRunner.query(`ALTER TABLE "chats_users_users" ADD CONSTRAINT "FK_3d891de1ee6dc86bb9d6c9f044e" FOREIGN KEY ("chatsId") REFERENCES "chats"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "chats_users_users" ADD CONSTRAINT "FK_91c62ffedcb3d34053b698b56e0" FOREIGN KEY ("usersId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "chats_users_users" DROP CONSTRAINT "FK_91c62ffedcb3d34053b698b56e0"`);
        await queryRunner.query(`ALTER TABLE "chats_users_users" DROP CONSTRAINT "FK_3d891de1ee6dc86bb9d6c9f044e"`);
        await queryRunner.query(`DROP INDEX "IDX_91c62ffedcb3d34053b698b56e"`);
        await queryRunner.query(`DROP INDEX "IDX_3d891de1ee6dc86bb9d6c9f044"`);
        await queryRunner.query(`DROP TABLE "chats_users_users"`);
    }

}
