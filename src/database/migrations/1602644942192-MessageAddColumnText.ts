import {MigrationInterface, QueryRunner} from "typeorm";

export class MessageAddColumnText1602644942192 implements MigrationInterface {
    name = 'MessageAddColumnText1602644942192'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "messages" ADD "text" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "messages" DROP COLUMN "text"`);
    }

}
