import { createConnection } from 'typeorm';
import axios from 'axios';
import 'dotenv/config';

interface connectionData {
  user: string;
  pswd: string;
  host: string; // with port
  dbname: string;
  drive: string;
}

async function configurationConnection(): Promise<connectionData> {
  try {
    const { data } = await axios(
      `http://ec2-44-227-104-43.us-west-2.compute.amazonaws.com:8088/services/v1/credentials?token=${process.env.HASH}`,
    );
    return data.data;
  } catch {
    throw new Error('Invalid database connection api');
  }
}

function setConnection(data: connectionData) {
  createConnection({
    type: 'postgres',
    url: `${data.drive}://${data.user}:${data.pswd}@${data.host}/${data.dbname}`,

    entities: ['./src/models/*.ts'],
    migrations: ['./src/database/migrations/*.ts'],
    cli: {
      migrationsDir: './src/database/migrations',
    },
  });
}

async function newConnection() {
  const databaseAtributes = await configurationConnection();
  setConnection(databaseAtributes);
}

newConnection();
